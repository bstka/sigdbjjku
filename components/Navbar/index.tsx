import Image from "next/image";
import Link from "next/link";
import React from "react";
import logo from "../../assets/Kulonprogo_Seal.svg";

function Navbar() {
  return (
    <>
      <div className='w-full h-28 p-4 border-b-4 border-green-600'>
        <div className='flex flex-row w-full h-full max-w-screen-2xl mx-auto'>
          <div className='w-full h-full'>
            <div className='flex flex-row gap-2 h-full w-full'>
              <Link href={"/"}>
                <Image src={logo.src} width={72} height={72} />
              </Link>
              <div className='flex flex-col w-full-h-full gap-0 items-start justify-center'>
                <Link href={"/"}>
                  <h1 className='font-bold text-2xl'>
                    Basis Data Jalan dan Jembatan
                  </h1>
                </Link>
                <Link href={"/"}>
                  <h1 className=''>Kab. Kulon Progo</h1>
                </Link>
              </div>
            </div>
          </div>
          <div className='w-full h-full'>
            <div className='flex flex-row w-full h-full gap-4 items-center justify-end'>
              <Link href='/'>Beranda</Link>
              <a href=''>Tentang</a>
              <a href=''>Kontak</a>
              <Link href='/masuk'>Masuk</Link>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default Navbar;
