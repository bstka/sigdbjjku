import { Vector } from "ol/source";
import { Vector as VectorLayer } from "ol/layer";
import { GeoJSON } from "ol/format";
import KulonProgoBorder from "../assets/kulonprogo.json";
import { Fill, Stroke, Style } from "ol/style";
import axios from "axios";

const KulonProgo_Source = new Vector({
  format: new GeoJSON({ featureProjection: "EPSG:3857" }),
  loader: async function (extent, resolution, projection, success, failure) {
    try {
      const features =
        KulonProgo_Source.getFormat()?.readFeatures(KulonProgoBorder);
      KulonProgo_Source.addFeatures(features as any);
      success!(features as any);
    } catch (error) {
      failure!();
    }
  },
});

const KulonProgo_Layer = new VectorLayer({
  source: KulonProgo_Source,
  style: new Style({
    fill: new Fill({
      color: "rgba(0,0,0,0)",
    }),
    stroke: new Stroke({
      color: "#ff0000",
      width: 3,
    }),
  }),
});

export { KulonProgo_Source, KulonProgo_Layer };
