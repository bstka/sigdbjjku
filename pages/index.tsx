import React, { useEffect, useRef } from "react";
import { Map as OLMap, View as OLView } from "ol";
import TileLayer from "ol/layer/Tile";
import { OSM } from "ol/source";
import * as Proj from "ol/proj";
import { KulonProgo_Layer } from "../libs/MapSource";
import KulonProgoBorder from "../assets/kulonprogo.json";

function Index() {
  const MapContainer = useRef<HTMLDivElement | null>(null);
  const MainMap = useRef<OLMap | null>(null);

  const MapView = new OLView({
    center: Proj.fromLonLat([110.16198363060715, -7.777775544975493]),
    maxZoom: 18,
    minZoom: 7,
    zoom: 12,
    extent: Proj.transformExtent(
      [109.9946286902, -8.01581095, 110.3070523962, -7.6008416983],
      "EPSG:4326",
      "EPSG:3857"
    ),
  });

  useEffect(() => {
    MainMap.current = new OLMap({
      target: MapContainer.current as HTMLElement,
      layers: [
        new TileLayer({
          source: new OSM(),
        }),
        KulonProgo_Layer,
      ],
      view: MapView,
    });

    (window as any).main = MainMap.current;
    return () => {
      MainMap.current?.setTarget(undefined);
      MainMap.current = null;
    };
  }, []);

  return (
    <>
      <div className='h-screen w-full'>
        <div className='w-full h-full flex flex-row items-center justify-start'>
          <div className='w-2/5 max-w-md h-full'>
            <div className='w-full h-full flex flex-col gap-2 items-center justify-start p-4'>
              <h1 className='font-semibold text-xl'>Statistik Basis Data</h1>
              <div className='w-full p-2'>
                <h1 className='text-lg font-semibold py-2'>Jumlah Objek:</h1>
                <div className='w-full h-36 flex flex-row gap-2'>
                  <div className='bg-gray-200 w-full h-full flex flex-col'>
                    <div className='bg-green-500 p-1'>
                      <h1 className='text-center font-semibold text-white'>
                        Ruas Jalan
                      </h1>
                    </div>
                    <div className='h-full w-full flex items-center justify-center'>
                      <h1 className='text-center font-bold text-4xl'>2500</h1>
                    </div>
                  </div>
                  <div className='bg-gray-200 w-full h-full flex flex-col'>
                    <div className='bg-blue-500 p-1'>
                      <h1 className='text-center font-semibold text-white'>
                        Jembatan
                      </h1>
                    </div>
                    <div className='h-full w-full flex items-center justify-center'>
                      <h1 className='text-center font-bold text-4xl'>1000</h1>
                    </div>
                  </div>
                </div>
              </div>
              <div className='w-full p-2'>
                <h1 className='text-lg font-semibold py-2'>Kelas Jalan:</h1>
                <div className='w-full h-32 flex flex-row gap-2'>
                  <div className='bg-gray-200 w-full h-full flex flex-col'>
                    <div className='bg-yellow-400 p-1'>
                      <h1 className='text-center font-semibold text-black'>
                        Nasional
                      </h1>
                    </div>
                    <div className='h-full w-full flex items-center justify-center'>
                      <h1 className='text-center font-bold text-4xl'>20</h1>
                    </div>
                  </div>
                  <div className='bg-gray-200 w-full h-full flex flex-col'>
                    <div className='bg-indigo-500 p-1'>
                      <h1 className='text-center font-semibold text-white'>
                        Provinsi
                      </h1>
                    </div>
                    <div className='h-full w-full flex items-center justify-center'>
                      <h1 className='text-center font-bold text-4xl'>90</h1>
                    </div>
                  </div>
                  <div className='bg-gray-200 w-full h-full flex flex-col'>
                    <div className='bg-cyan-500 p-1'>
                      <h1 className='text-center font-semibold text-white'>
                        Kabupaten
                      </h1>
                    </div>
                    <div className='h-full w-full flex items-center justify-center'>
                      <h1 className='text-center font-bold text-4xl'>1000</h1>
                    </div>
                  </div>
                </div>
              </div>
              <div className='w-full p-2'>
                <h1 className='text-lg font-semibold py-2'>Kondisi Jalan:</h1>
                <div className='w-full h-32 flex flex-row gap-2'>
                  <div className='bg-gray-200 w-full h-full flex flex-col'>
                    <div className='bg-green-500 p-1'>
                      <h1 className='text-center font-semibold text-white'>
                        Baik
                      </h1>
                    </div>
                    <div className='h-full w-full flex items-center justify-center'>
                      <h1 className='text-center font-bold text-4xl'>2450</h1>
                    </div>
                  </div>
                  <div className='bg-gray-200 w-full h-full flex flex-col'>
                    <div className='bg-red-500 p-1'>
                      <h1 className='text-center font-semibold text-white'>
                        Rusak
                      </h1>
                    </div>
                    <div className='h-full w-full flex items-center justify-center'>
                      <h1 className='text-center font-bold text-4xl'>50</h1>
                    </div>
                  </div>
                </div>
              </div>
              <div className='w-full p-2'>
                <h1 className='text-lg font-semibold py-2'>
                  Kondisi Jembatan:
                </h1>
                <div className='w-full h-32 flex flex-row gap-2'>
                  <div className='bg-gray-200 w-full h-full flex flex-col'>
                    <div className='bg-green-500 p-1'>
                      <h1 className='text-center font-semibold text-white'>
                        Baik
                      </h1>
                    </div>
                    <div className='h-full w-full flex items-center justify-center'>
                      <h1 className='text-center font-bold text-4xl'>50</h1>
                    </div>
                  </div>
                  <div className='bg-gray-200 w-full h-full flex flex-col'>
                    <div className='bg-red-500 p-1'>
                      <h1 className='text-center font-semibold text-white'>
                        Rusak
                      </h1>
                    </div>
                    <div className='h-full w-full flex items-center justify-center'>
                      <h1 className='text-center font-bold text-4xl'>20</h1>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className='w-full h-full' ref={MapContainer}></div>
        </div>
      </div>
    </>
  );
}

export default Index;
