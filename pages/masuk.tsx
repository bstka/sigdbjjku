import React from "react";

function Masuk() {
  return (
    <>
      <div className='w-full' style={{ height: "calc(100vh - 112px)" }}>
        <div className='w-full h-full flex items-center justify-center'>
          <div className='w-96 h-96'>
            <div className='flex flex-col w-full h-full gap-8'>
              <h1 className='text-center font-semibold text-4xl'>Masuk</h1>
              <div className='flex flex-col gap-8 w-full h-full'>
                <div className='w-full flex flex-col gap-2 justify-start items-start'>
                  <p className='tracking-wider font-semibold text-sm'>
                    Nama Pengguna
                  </p>
                  <div className='w-full flex flex-row gap-4 items-center '>
                    <span className='material-icons-outlined'>person</span>
                    <input
                      type='text'
                      className='border-b-2 border-green-500 p-2 outline-none w-full'
                    />
                  </div>
                </div>
                <div className='w-full flex flex-col gap-2 justify-start items-start'>
                  <p className='tracking-wider font-semibold text-sm'>
                    Kata Sandi
                  </p>
                  <div className='w-full flex flex-row gap-4 items-center '>
                    <span className='material-icons-outlined'>lock</span>
                    <input
                      type='password'
                      className='border-b-2 border-green-500 p-2 outline-none w-full'
                    />
                  </div>
                </div>
                <div className='w-full flex flex-row justify-end'>
                  <button className='bg-green-500 py-2 px-4 font-semibold text-white tracking-wide flex items-center justify-center gap-2'>
                    <span className='material-icons-outlined'>login</span>
                    <p>Masuk</p>
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default Masuk;
